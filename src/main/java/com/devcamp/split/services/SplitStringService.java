package com.devcamp.split.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class SplitStringService {
    public ArrayList<String> splitString(String inputString) {
        System.out.println("Splitting string:" + inputString);

        String[] arrayString = inputString.split("[.\s,]\s{0,1}");

        ArrayList<String> arrayResult = new ArrayList<>();
        for (String word: arrayString) {
            arrayResult.add(word);
        }

        return arrayResult;        
    }
}
