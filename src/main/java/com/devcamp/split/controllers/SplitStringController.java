package com.devcamp.split.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.split.services.SplitStringService;

@RestController
public class SplitStringController {
    @Autowired
    private SplitStringService splitStringService;

    @GetMapping("/split")
    public ResponseEntity<ArrayList<String>> splitString(@RequestParam String inputString) {
        return new ResponseEntity<>(this.splitStringService.splitString(inputString), HttpStatus.OK);
    }
}
